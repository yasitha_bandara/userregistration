package com.example.demo.services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.models.Image;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.security.TokenAuthenticationService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ObjectBuffer;

//import net.minidev.json.JSONObject;

@Service
public class UserService 
{
	@Autowired
	private UserRepository userRepo; 
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private  mailService mail_Service;
	
	@Value("${defaultProfileImageId}")
	private String defaultProfileImageId;
	
	@Value("${Image.filePath}")
	private String imageFilePath;
	
	public String dummyReturn()
	{
		User tst=new User();
        tst.setName("hooi"); 
        userRepo.save(tst);
		return "salalalalaa";
	}
	public User registerUser(User user)  
	{
		User userWithMail = userRepo.findByEmail(user.getEmail());
		
		if(userWithMail==null)
		{			
			System.out.println("no records in given email ");
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(hashedPassword);
			
			Image defaultImage=new Image();
			defaultImage.setId(defaultProfileImageId);
			defaultImage.setName("default");
			defaultImage.setProfile(true);
			List<Image> images=new ArrayList<Image>();
			images.add(defaultImage);
			user.setImages(images);
			
			user.setEmailverificationStatus(false);
			user.setVerificationemailSendingStatus(true);
			Date verificationLinkExpDate= new Date();
			verificationLinkExpDate.setTime(verificationLinkExpDate.getTime() + (8 * 24 * 60 * 60 * 1000));
			user.setVerificationLinkExpDate(verificationLinkExpDate);
			userRepo.save(user);
			mail_Service.sendVerificationEmail(user);
		}
		return user;
	}

	public User getUserByName(String name)
	{
		List<User> users = userRepo.findUsersByName(name);
		if (users.size()==0)return null;
		return users.get(0);
	}
	//public boolean saveFile(MultipartFile file,HttpServletRequest request)
	public boolean saveFile(String userId,String imageName,MultipartFile file)
	{
	    //Authentication authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest)request);
		//String username=authentication.getName();
		
		User user=userRepo.findOne(userId);
		Image newImage=new Image();
		newImage.setId(new ObjectId().toString());
		newImage.setName(imageName);
		File dir=null;
		if (!file.isEmpty())
		{ 
			System.out.println("!file.isEmpty"); 
			try
			{
				String filePath =  imageFilePath + newImage.getId() +".jpg"; 
				file.transferTo(new File(filePath));
			}
			catch(Exception ex){
				 System.out.println(ex);
			}
			if(user.getImages()==null)
			{
				System.out.println("user.getImages()==null");
				user.setImages(new ArrayList<Image>());
			}
			
			user.getImages().add(newImage);
			userRepo.save(user);
		}
		return true;
	}
	public boolean sendGmail()
	{
		SimpleMailMessage mail=new SimpleMailMessage();
		mail.setTo("yasitha.dev@yahoo.com");
		mail.setFrom("yasitha.dev@gmail.com");
		mail.setSubject("subject eka");
		mail.setText("this is the content");
		mailSender.send(mail);
		return true;
	}
	public boolean sendYahooMail()
	{
	/*	Mail email = new SimpleMailMessage( "user@domain.com", "Teste", "teste..", null );
        email.setHostName( "smtp.mail.yahoo.com" );
        email.setSmtpPort( 587 );
        email.setAuthentication( "user@yahoo.com.br", "mypassword" );
        email.setFrom( "user@yahoo.com.br", "My Name" );
        email.send();*/
		return true;
	}
	public User completeProfile(User user) 
	{
		// TODO Auto-generated method stub
		for (int i = 0; i < user.getImages().size(); i++) 
		{
			user.getImages().get(i).setId(new ObjectId().toString());
		}
		User insertableUser = userRepo.findOne(user.getId());
		insertableUser.setImages(user.getImages());
		userRepo.save(insertableUser);
		return insertableUser;
	}
	public User verifyEmail(String userid) 
	{
		User user=userRepo.findOne(userid);
		if(user!=null)
		{
			if(!user.getEmailverificationStatus())
			{
				Date today= new Date();
				Date linkExpDate= user.getVerificationLinkExpDate();
				if(today.compareTo(linkExpDate) < 0){
					user.setEmailverificationStatus(true);
					userRepo.save(user);
				}
				else{
					//link has Expired;
				}
			}
			else{
				//Email is already veryfied 
			}
		}
		else{
			//user not found
		}
		return null;
	}
}
