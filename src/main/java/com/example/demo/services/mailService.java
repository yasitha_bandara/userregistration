package com.example.demo.services;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.example.demo.models.User;

@Component
public class mailService 
{
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	private MailSender mailSender;
	
	
	public void sendJavaMailSenderMail() throws MessagingException
	{
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message,true);
		helper.setSubject("This is the subject");
		helper.setTo("yasitha.dev@yahoo.com");
		helper.setText("Body of the Mail",true);
	
		javaMailSender.send(message);
	}

	public void sendSpringMailSendermail() 
	{
		SimpleMailMessage mail=new SimpleMailMessage();
		mail.setTo("yasitha.dev@yahoo.com");
		mail.setFrom("yasitha.dev@gmail.com");
		mail.setSubject("subject eka");
		SimpleMailMessage msg=new SimpleMailMessage(mail);
		msg.setText("this is the content");
		
		mailSender.send(msg);
		
	}
	public void sendVerificationEmail(User user) 
	{
		SimpleMailMessage email=new SimpleMailMessage();
		email.setTo(user.getEmail());
		email.setFrom("yasitha.dev@gmail.com");
		email.setSubject("Please verify your email address");
		SimpleMailMessage msg=new SimpleMailMessage(email);
		msg.setText("http://localhost:8080/verifyemail/" + user.getId());
		mailSender.send(msg);		
	}
}
