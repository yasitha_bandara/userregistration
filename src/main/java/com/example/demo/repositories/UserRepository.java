package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.models.User;


@Repository
public interface UserRepository extends MongoRepository<User,String>
{
	@Query("{ 'name' : ?0 }")
	List<User> findUsersByName(String name);
	
	User findByName(String name);
	
	User findByEmail(String Email);
}
