package com.example.demo.controllers;

import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import static org.springframework.http.HttpStatus.*;

import com.example.demo.services.UserService;
import com.example.demo.services.mailService;
import com.example.demo.models.User;
@RestController
public class UserController
{
	@Autowired
	private UserService userService;
	
	@Autowired
	private mailService mail_Service;
	
	@RequestMapping("/users")
	public @ResponseBody String greeting()
	{
		return "{\"users\":[{\"firstname\":\"Richard\", \"lastname\":\"Feynman\"}," +
		           "{\"firstname\":\"Marie\",\"lastname\":\"Curie\"}]}";
	}
	
	@RequestMapping(value = "/signup",method = RequestMethod.POST)
	public ResponseEntity<User> signup(@RequestBody User user)
	{
		User user2 = userService.registerUser(user);
		return new ResponseEntity<User>( user2,HttpStatus.OK);
	}
	
	@RequestMapping(value="/uploadimage/{userId}/{imageName}",method = RequestMethod.POST)
	//public @ResponseBody String updateInfo(HttpServletRequest request,@RequestParam("file")MultipartFile file)
	public @ResponseBody String updateInfo(@PathVariable String userId,@PathVariable String imageName,@RequestParam("file")MultipartFile file)
	{
		//userService.saveFile(file,request);
		userService.saveFile(userId,imageName,file);
		return "success";
	}
	
	@RequestMapping(value="/sendgmail",method = RequestMethod.GET)
	public @ResponseBody HttpStatus sendGmail()
	{
		userService.sendGmail();
		return (OK);
	}
	@RequestMapping(value="/sendyahoomail",method = RequestMethod.GET)
	public @ResponseBody HttpStatus sendYahooMail()
	{
		userService.sendYahooMail();
		return (OK);
	}
	
	@RequestMapping(value="/sendjavamailsendermail",method = RequestMethod.GET)
	public @ResponseBody HttpStatus sendJavaMailSenderMail()
	{
		try 
		{
			this.mail_Service.sendJavaMailSenderMail();
		} 
		catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (OK);
	}
	@RequestMapping(value="/sendspringmailsendermail",method = RequestMethod.GET)
	public ResponseEntity<String> sendSpringMailSendermail()
	{
		mail_Service.sendSpringMailSendermail();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@RequestMapping(value="/userbyname/{uname}",method = RequestMethod.GET)
	public @ResponseBody User showUser(@PathVariable String uname)
	{
		return userService.getUserByName(uname);
	}
	
	@RequestMapping("/completeprofile")
	public User completeProfile(@RequestBody User user)
	{
		return userService.completeProfile(user);
	}
	@RequestMapping("/verifyemail/{userid}")
	public User verifyEmail(@PathVariable String userid)
	{
		return userService.verifyEmail(userid);
	}
}
