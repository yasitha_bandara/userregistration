package com.example.demo.models;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


//@JsonIgnoreProperties(value = { "password" })//jakson annotation
@Document
public class User 
{
	@Id
	private String id;
	private String name;
	private String email;
	private String password;
	private List<Image> images;
	private boolean emailverificationStatus;
	private boolean verificationemailSendingStatus;
	private Date verificationLinkExpDate;
	
	public Date getVerificationLinkExpDate() {
		return verificationLinkExpDate;
	}
	public void setVerificationLinkExpDate(Date verificationLinkExpDate) {
		this.verificationLinkExpDate = verificationLinkExpDate;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean getEmailverificationStatus() {
		// TODO Auto-generated method stub
		return this.emailverificationStatus;
	}
	public void setEmailverificationStatus(boolean b) {
		this.emailverificationStatus = b;
	}
	public boolean getVerificationemailSendingStatus() {
		return verificationemailSendingStatus;
	}
	public void setVerificationemailSendingStatus(boolean verificationemailSendingStatus) {
		this.verificationemailSendingStatus = verificationemailSendingStatus;
	}
}
