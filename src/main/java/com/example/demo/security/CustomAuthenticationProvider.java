package com.example.demo.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Predicate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserRepository userRepo;

    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        System.out.println("incoming username "+ name);
        System.out.println("incoming password "+ password);
        
        List<User> users = userRepo.findUsersByName(name);

       if(users.get(0)!= null)
       {   
    	   System.out.println("stored username "+ users.get(0).getName());
       		System.out.println("stored password "+ users.get(0).getPassword());
			
       		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(password);
			System.out.println("hashed password "+ hashedPassword);
			if(hashedPassword.equals(users.get(0).getPassword())){
				System.out.println("mached");
    	   return new UsernamePasswordAuthenticationToken(users.get(0).getName(),users.get(0).getPassword(), new ArrayList<>());
			}
			System.out.println("didn't mached");
		}
         else
         {
        	 System.out.println("-username null"); 
         }
       return null;
            // use the credentials
            // and authenticate against the third-party system
            

    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
          UsernamePasswordAuthenticationToken.class);
    }
}
