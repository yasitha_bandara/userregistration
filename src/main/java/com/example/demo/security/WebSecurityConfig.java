package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@ComponentScan("com.example.demo.security")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl; 
	
    @Override
	  protected void configure(HttpSecurity http) throws Exception 
	  {
    	http.csrf().disable().authorizeRequests()
	        .antMatchers("/").permitAll()
	        .antMatchers(HttpMethod.POST, "/login").permitAll()
	        .antMatchers(HttpMethod.POST, "/signup").permitAll()
	        .antMatchers("/verifyemail/{\\d+}").permitAll()
	        .anyRequest().authenticated()
	        .and()
	        // We filter the api/login requests
	        .addFilterBefore(new JWTLoginFilter("/login", authenticationManager(),userDetailsServiceImpl),
	                UsernamePasswordAuthenticationFilter.class)
	        // And filter other requests to check the presence of JWT in header
	        .addFilterBefore(new JWTAuthenticationFilter(),
	                UsernamePasswordAuthenticationFilter.class);
	  }

	/*  @Override
	  protected void configure(AuthenticationManagerBuilder auth) throws Exception 
	  {
	    //// Create a default account
	    //auth.inMemoryAuthentication()
	      //  .withUser("admin")
	        //.password("password")
	        //.roles("ADMIN");
		 auth.authenticationProvider(authProvider);	  
	  }*/
	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.authenticationProvider(authProvider());
	    }
	    @Bean
	    public DaoAuthenticationProvider authProvider() {
	        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	        authProvider.setUserDetailsService(userDetailsServiceImpl);
	        authProvider.setPasswordEncoder(encoder());
	        return authProvider;
	    }
	    @Bean
	    public PasswordEncoder encoder() {
	        return new BCryptPasswordEncoder();
	}
}

