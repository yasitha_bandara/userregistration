package com.example.demo.security;

import com.example.demo.models.User;
import com.example.demo.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter{

UserDetailsServiceImpl userDetailsServiceImpl;
	//getUserByName(String name)
  public JWTLoginFilter(String url, AuthenticationManager authManager,UserDetailsServiceImpl uds) 
  {
    super(new AntPathRequestMatcher(url));
    setAuthenticationManager(authManager);
    this.userDetailsServiceImpl=uds;
  }
  @Override
  public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)throws AuthenticationException, IOException, ServletException 
  {
    AccountCredentials creds = new ObjectMapper().readValue(req.getInputStream(), AccountCredentials.class);
    return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(creds.getUsername(),creds.getPassword(),Collections.emptyList()));
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest req,HttpServletResponse res, FilterChain chain,Authentication auth) throws IOException, ServletException 
  {
    TokenAuthenticationService.addAuthentication(res, auth.getName());
    User user= userDetailsServiceImpl.getUserByName(auth.getName());
    
    res.setContentType("application/json");
    
    String userJson= new ObjectMapper().writeValueAsString(user);
    
    res.getWriter().write(userJson);
  }
}
